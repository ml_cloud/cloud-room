package com.ml.room.repository.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @ClassName: BuildingRecordDto
 * @Decription: 此处填写类文件说明
 * @Author: IDai
 * @Date: 2021-12-27 13:34 星期一
 **/
@Data
public class BuildingRecordDto implements Serializable {

    private String buildingName;

    private Long page;

    private Long size;

}

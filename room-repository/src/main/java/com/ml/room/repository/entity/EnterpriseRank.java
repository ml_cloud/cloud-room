package com.ml.room.repository.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName: EnterpriseRank
 * @Decription: 【云房平台】 - 企业排行 - 数据实体
 * @Author: IDai
 * @Date: 2021-08-27 16:05 星期五
 **/
@Data
@TableName(value = "enterprise_rank")
public class EnterpriseRank implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "企业名称")
    private String enterpriseName;

    @ApiModelProperty(value = "点赞量")
    private Integer supportCount;

    @ApiModelProperty(value = "关注量")
    private Integer attentionCount;

    @ApiModelProperty(value = "企业类型（1：开发公司，2：设计公司，3：物业公司）")
    private Integer enterpriseType;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createDate;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    private Date updateDate;

    @ApiModelProperty(value = "状态（0：正常，1：异常）")
    private Integer status;

}

package com.ml.room.repository.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName: SysLog
 * @Decription: 此处填写类文件说明
 * @Author: xjs
 * @Date: 2021-05-20 16:24 星期四
 **/
@Data
@TableName(value = "SYS_EXCEPTION_LOG")
public class SysExceptionLog implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 唯一id
     */
    @TableId(type = IdType.ID_WORKER_STR)
    private String id;

    /**
     * 请求方法
     */
    private String method;

    /**
     * 请求参数
     */
    private String requestParam;

    /**
     * ip地址
     */
    private String ip;

    /**
     * 操作人用户账户
     */
    private String username;

    /**
     * 操作人用户名称
     */
    private String nickName;

    /**
     * 异常路由
     */
    private String uri;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 异常名
     */
    private String exception;
}

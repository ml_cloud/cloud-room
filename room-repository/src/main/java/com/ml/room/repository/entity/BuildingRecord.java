package com.ml.room.repository.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @ClassName: BuildingRecord
 * @Decription: 【云房平台】 - 房屋备案 - 数据实体
 * @Author: IDai
 * @Date: 2021-08-13 14:41 星期五
 **/
@Data
@TableName(value = "building_record")
@ApiModel(value = "楼盘备案数据")
public class BuildingRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(type = IdType.ID_WORKER_STR)
    private String id;

    @ApiModelProperty(value = "备案号")
    private String remarkNo;

    @ApiModelProperty(value = "楼盘名称")
    private String buildingName;

    @ApiModelProperty(value = "楼盘号")
    private String buildingNo;

    @ApiModelProperty(value = "建筑面积")
    private String buildingArea;

    @ApiModelProperty(value = "套数")
    private Integer setNumber;

    @ApiModelProperty(value = "均价")
    private String averagePrice;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createDate;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    private Date updateDate;

    @ApiModelProperty(value = "状态（0：正常，1：异常）")
    private Integer status;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "关键词")
    private String keywords;

    @ApiModelProperty(value = "楼盘详情地址")
    private String infoUrl;

    @ApiModelProperty(value = "详情抓取状态")
    private Integer infoStatus;

    @ApiModelProperty(value = "项目抓取状态")
    private Integer projectStatus;

    @ApiModelProperty(value = "抓取页码")
    private Integer collarPage;

    @TableField(exist = false)
    private BuildingProject buildingProject;

    @TableField(exist = false)
    private List<BuildingProjectItem> projectItems;
}

package com.ml.room.repository.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName: SysLog
 * @Decription: 此处填写类文件说明
 * @Author: xjs
 * @Date: 2021-05-20 16:24 星期四
 **/
@Data
@TableName(value = "SYS_LOG")
public class SysLog implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 唯一id
     */
    @TableId(type = IdType.ID_WORKER_STR)
    private String id;

    /**
     * 日志类型(1:登录日志;2:操作日志)
     */
    private String logType;

    /**
     * 内容
     */
    private String logContent;

    /**
     * 请求方法
     */
    private String method;

    /**
     * 操作类型(1:添加;2:删除;3:修改;4:业务代码)
     */
    private String operateType;

    /**
     * 请求参数
     */
    private String requestParam;

    /**
     * ip地址
     */
    private String ip;

    /**
     * 操作人用户账户
     */
    private String username;

    /**
     * 操作人用户名称
     */
    private String nickName;

    /**
     * 程序执行时间
     */
    private Long costTime;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 请求路由
     */
    private String uri;
}

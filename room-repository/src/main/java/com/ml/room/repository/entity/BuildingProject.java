package com.ml.room.repository.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName: BuildingProject
 * @Decription: 【云房平台】 - 房建项目 - 数据实体
 * @Author: IDai
 * @Date: 2021-08-27 11:38 星期五
 **/
@Data
@TableName(value = "building_project")
@ApiModel(value = "楼盘项目数据")
public class BuildingProject implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(type = IdType.ID_WORKER_STR)
    private String id;

    @ApiModelProperty(value = "项目名称")
    private String projectName;

    @ApiModelProperty(value = "坐落位置")
    private String seatingPosition;

    @ApiModelProperty(value = "所在区域")
    private String locationArea;

    @ApiModelProperty(value = "物业类别")
    private String propertyClass;

    @ApiModelProperty(value = "建筑类型")
    private String buildingTypes;

    @ApiModelProperty(value = "开发企业")
    private String developEnterprise;

    @ApiModelProperty(value = "设计单位")
    private String designUnit;

    @ApiModelProperty(value = "物业公司")
    private String propertyCompany;

    @ApiModelProperty(value = "周边配套")
    private String circumMating;

    @ApiModelProperty(value = "项目信息")
    private String projectInfo;

    @ApiModelProperty(value = "交通状况")
    private String trafficCondition;

    @ApiModelProperty(value = "项目备注说明")
    private String projectRemark;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createDate;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    private Date updateDate;

    @ApiModelProperty(value = "状态（0：正常，1：异常）")
    private Integer status;

    @ApiModelProperty(value = "外键（房屋备案主键id）")
    private String recordId;


}

package com.ml.room.repository.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName: SysCollarConfig
 * @Decription: 此处填写类文件说明
 * @Author: IDai
 * @Date: 2021-08-26 10:45 星期四
 **/
@Data
@TableName(value = "sys_collar_config")
public class SysCollarConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(type = IdType.ID_WORKER_STR)
    private String id;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "城市")
    private String city;

    @ApiModelProperty(value = "前戳地址")
    private String prefixUrl;

    @ApiModelProperty(value = "抓取地址")
    private String collarUrl;

    @ApiModelProperty(value = "创建时间")
    private Date createDate;

    @ApiModelProperty(value = "更新时间")
    private Date updateDate;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "抓取页码")
    private Integer collarPage;

    @ApiModelProperty(value = "状态（0：正常，1：异常）")
    private Integer status;

}

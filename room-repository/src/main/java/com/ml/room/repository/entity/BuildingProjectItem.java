package com.ml.room.repository.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName: BuildingProjectItem
 * @Decription: 【云房平台】 - 房建项目楼房 - 数据库实体
 * @Author: IDai
 * @Date: 2021-08-27 11:58 星期五
 **/
@Data
@TableName(value = "building_project_item")
@ApiModel(value = "楼房单元数据")
public class BuildingProjectItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(type = IdType.ID_WORKER_STR)
    private String id;

    @ApiModelProperty(value = "楼号")
    private String buildingNo;

    @ApiModelProperty(value = "房号")
    private String roomNumber;

    @ApiModelProperty(value = "户型")
    private String houseType;

    @ApiModelProperty(value = "建筑面积")
    private String roomArea;

    @ApiModelProperty(value = "公摊面积")
    private String commonArea;

    @ApiModelProperty(value = "套内面积")
    private String caseArea;

    @ApiModelProperty(value = "备案单价")
    private String recordPrice;

    @ApiModelProperty(value = "备案总价")
    private String recordAmount;

    @ApiModelProperty(value = "房屋属性")
    private String buildingProperty;

    @ApiModelProperty(value = "装修情况")
    private String decorateState;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "房屋状态（0：正常，1：异常）")
    private int roomStatus;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createDate;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    private Date updateDate;

    @ApiModelProperty(value = "房建备案id（外键）")
    private String recordId;

    @ApiModelProperty(value = "房屋项目id（外键）")
    private String projectId;

}

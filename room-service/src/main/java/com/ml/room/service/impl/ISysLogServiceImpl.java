package com.ml.room.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ml.room.dao.ISysLogDao;
import com.ml.room.repository.entity.SysLog;
import com.ml.room.service.ISysLogService;
import org.springframework.stereotype.Service;

/**
 * @ClassName: ISysLogServiceImpl
 * @Decription: 此处填写类文件说明
 * @Author: IDai
 * @Date: 2021-08-06 11:44 星期五
 **/
@Service
public class ISysLogServiceImpl extends ServiceImpl<ISysLogDao, SysLog> implements ISysLogService {
}

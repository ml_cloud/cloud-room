package com.ml.room.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ml.room.repository.entity.SysCollarConfig;

/**
 * @ClassName: ISysCollarConfigService
 * @Decription: 此处填写类文件说明
 * @Author: IDai
 * @Date: 2021-08-26 10:52 星期四
 **/
public interface ISysCollarConfigService extends IService<SysCollarConfig> {
}

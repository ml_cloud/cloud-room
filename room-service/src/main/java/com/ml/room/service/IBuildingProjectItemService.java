package com.ml.room.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ml.room.repository.entity.BuildingProjectItem;

/**
 * @ClassName: IBuildingProjectItemService
 * @Decription: 【云房平台】 - 房建项目楼房 - service接口层
 * @Author: IDai
 * @Date: 2021-08-27 15:58 星期五
 **/
public interface IBuildingProjectItemService extends IService<BuildingProjectItem> {
}

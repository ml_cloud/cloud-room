package com.ml.room.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ml.room.dao.ISysCollarConfigDao;
import com.ml.room.repository.entity.SysCollarConfig;
import com.ml.room.service.ISysCollarConfigService;
import org.springframework.stereotype.Service;

/**
 * @ClassName: ISysCollarConfigServiceImpl
 * @Decription: 此处填写类文件说明
 * @Author: IDai
 * @Date: 2021-08-26 10:52 星期四
 **/
@Service
public class ISysCollarConfigServiceImpl extends ServiceImpl<ISysCollarConfigDao,SysCollarConfig> implements ISysCollarConfigService {
}

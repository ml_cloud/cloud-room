package com.ml.room.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ml.room.repository.entity.SysLog;

/**
 * @ClassName: ISysLogService
 * @Decription: 此处填写类文件说明
 * @Author: IDai
 * @Date: 2021-08-06 11:42 星期五
 **/
public interface ISysLogService extends IService<SysLog> {
}

package com.ml.room.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ml.room.dao.ISysExceptionLogDao;
import com.ml.room.repository.entity.SysExceptionLog;
import com.ml.room.service.ISysExceptionLogService;
import org.springframework.stereotype.Service;

/**
 * @ClassName: ISysExceptionLogServiceImpl
 * @Decription: 此处填写类文件说明
 * @Author: IDai
 * @Date: 2021-08-06 11:47 星期五
 **/
@Service
public class ISysExceptionLogServiceImpl extends ServiceImpl<ISysExceptionLogDao, SysExceptionLog> implements ISysExceptionLogService {
}

package com.ml.room.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ml.room.dao.IBuildingProjectDao;
import com.ml.room.repository.entity.BuildingProject;

/**
 * @ClassName: IBuildingProjectService
 * @Decription: 【云房平台】 - 房建项目 - service接口层
 * @Author: IDai
 * @Date: 2021-08-27 11:51 星期五
 **/
public interface IBuildingProjectService extends IService<BuildingProject> {
}

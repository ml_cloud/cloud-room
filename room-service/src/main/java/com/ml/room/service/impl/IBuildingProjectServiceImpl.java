package com.ml.room.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ml.room.dao.IBuildingProjectDao;
import com.ml.room.repository.entity.BuildingProject;
import com.ml.room.service.IBuildingProjectService;
import com.ml.room.service.IBuildingRecordService;
import org.springframework.stereotype.Service;

/**
 * @ClassName: IBuildingProjectServiceImpl
 * @Decription: 【云房平台】 - 房建项目 - service业务层
 * @Author: IDai
 * @Date: 2021-08-27 11:55 星期五
 **/
@Service
public class IBuildingProjectServiceImpl extends ServiceImpl<IBuildingProjectDao, BuildingProject> implements IBuildingProjectService {


}

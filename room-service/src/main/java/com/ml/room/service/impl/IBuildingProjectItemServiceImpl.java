package com.ml.room.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ml.room.dao.IBuildingProjectItemDao;
import com.ml.room.repository.entity.BuildingProject;
import com.ml.room.repository.entity.BuildingProjectItem;
import com.ml.room.service.IBuildingProjectItemService;
import com.ml.room.service.IBuildingProjectService;
import org.springframework.stereotype.Service;

/**
 * @ClassName: IBuildingProjectItemServiceImpl
 * @Decription: 【云房平台】 - 房建项目楼房 - service业务层
 * @Author: IDai
 * @Date: 2021-08-27 15:59 星期五
 **/
@Service
public class IBuildingProjectItemServiceImpl extends ServiceImpl<IBuildingProjectItemDao, BuildingProjectItem> implements IBuildingProjectItemService {
}

package com.ml.room.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ml.room.common.result.CommonResult;
import com.ml.room.repository.dto.BuildingRecordDto;
import com.ml.room.repository.entity.BuildingRecord;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: IBuildingRecordService
 * @Decription: 【云房平台】 - 房屋备案
 * @Author: IDai
 * @Date: 2021-08-13 14:46 星期五
 **/
public interface IBuildingRecordService extends IService<BuildingRecord> {

    /**
     * method selectNotCollarList
     * create by: IDai
     * description: 查询出还未爬取的数据列表
     * create time: 2021/8/30 0030
     */
    IPage<BuildingRecord> selectNotCollarList(Map<String, String> collarMap);


    /**
     * method findBuildingRecordInfo
     * create by: IDai
     * description: 根据id查询房屋备案 - 房屋项目 - 楼房详情等信息
     * create time: 2021/9/6 0006
     */
    BuildingRecord findBuildingRecordInfo(String remarkNo);

    /**
     * method
     * create by: IDai
     * description: 根据楼盘名称模糊分页查询楼盘列表
     * create time: 2021/12/27 0027
     */
    CommonResult<IPage<BuildingRecord>> selectBuildingList(BuildingRecordDto buildingRecordDto);

    /**
     * method selectHotSearch
     * create by: IDai
     * description: 【云房平台】 - 热搜列表
     * create time: 2021/10/20 0020
     */
    CommonResult<?> selectHotSearch();

    /**
     * method selectHotSearchBuildingRecords
     * create by: IDai
     * description: [云房平台] - 热搜楼房数据
     * create time: 2021/10/22 0022
     */
    CommonResult<?> selectHotSearchBuildingRecords();

    /**
     * method findBuildingGlobalInfo
     * create by: IDai
     * description: [云房平台] - 楼盘大致信息
     * create time: 2021/10/22 0022
     */
    CommonResult<?> findBuildingGlobalInfo(Map<String, Object> searchMap);

}

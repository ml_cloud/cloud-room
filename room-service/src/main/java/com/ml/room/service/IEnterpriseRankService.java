package com.ml.room.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ml.room.repository.entity.EnterpriseRank;

/**
 * @ClassName: IEnterpriseRankService
 * @Decription: 【云房平台】 - 企业排行 - service接口层
 * @Author: IDai
 * @Date: 2021-08-27 16:09 星期五
 **/
public interface IEnterpriseRankService extends IService<EnterpriseRank> {
}

package com.ml.room.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ml.room.dao.IEnterpriseRankDao;
import com.ml.room.repository.entity.EnterpriseRank;
import com.ml.room.service.IEnterpriseRankService;
import org.springframework.stereotype.Service;

/**
 * @ClassName: IEnterpriseRankServiceImpl
 * @Decription: 【云房平台】 - 企业排行 - service业务层
 * @Author: IDai
 * @Date: 2021-08-27 16:09 星期五
 **/
@Service
public class IEnterpriseRankServiceImpl extends ServiceImpl<IEnterpriseRankDao, EnterpriseRank> implements IEnterpriseRankService {
}

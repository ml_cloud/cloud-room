package com.ml.room.terrace.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @ClassName: SwaggerConfig
 * @Decription: 此处填写类文件说明
 * @Author: IDai
 * @Date: 2021-09-06 11:12 星期一
 **/
@Component
public class SwaggerConfig implements ApplicationListener<WebServerInitializedEvent> {

    /** 日志打印工具 **/
    private static final Logger logger = LoggerFactory.getLogger(SwaggerConfig.class);
    private int serverPort;

    public int getPort() {
        return this.serverPort;
    }

    @Override
    public void onApplicationEvent(WebServerInitializedEvent event) {
        try {
            InetAddress inetAddress = Inet4Address.getLocalHost();
            this.serverPort = event.getWebServer().getPort();
            logger.info("项目启动启动成功！接口文档地址: http://"+inetAddress.getHostAddress()+":"+serverPort+"/swagger-ui.html");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}

package com.ml.room.terrace.component;

import java.lang.annotation.*;

/**
 * @ClassName: AutoLog
 * @Decription: 此处填写类文件说明
 * @Author: xjs
 * @Date: 2021-05-20 13:38 星期四
 **/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AutoLog {

    /**
     * 日志内容
     *
     * @return
     */
    String value() default "";

    /**
     * 日志类型
     *
     * @return 1登录日志，2操作日志
     */
    String logType() default "2";

    /**
     * 操作类型
     *
     * @return 1:添加;2:删除;3:修改;4:业务代码
     */
    String operateType() default "1";
}

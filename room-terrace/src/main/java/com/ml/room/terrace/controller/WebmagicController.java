package com.ml.room.terrace.controller;

import com.alibaba.fastjson.JSON;
import com.ml.room.repository.entity.BuildingRecord;
import com.ml.room.service.IBuildingRecordService;
import com.ml.room.terrace.process.BuildingRecordPageProcess;
import com.ml.room.common.result.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: WebmagicController
 * @Decription: 此处填写类文件说明
 * @Author: IDai
 * @Date: 2021-08-25 10:36 星期三
 **/
@RestController
@RequestMapping("/api/building")
@Api(value = "[合肥新房] - 备案房建 - 控制层", tags = "房建信息综合服务")
public class WebmagicController {

    @Autowired
    private BuildingRecordPageProcess processor;
    @Autowired
    private RestHighLevelClient restHighLevelClient;
    @Autowired
    private IBuildingRecordService buildingRecordService;

    @ApiOperation(httpMethod = "POST", value = "新闻信息爬取")
    @ApiImplicitParam(name = "remarkNo",value = "备案号",example = "202012068")
    @RequestMapping(value = "/buildingRecord.ml", method = { RequestMethod.GET }, produces = "application/json;charset=UTF-8")
    public CommonResult<?> buildingRecord() throws IOException {
        List<BuildingRecord> buildingRecords = new ArrayList<>();
        SearchRequest request = new SearchRequest("jdbc_c");
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        //条件构造器
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        TermQueryBuilder termQueryBuilder = new TermQueryBuilder("remark_no", "201202015");
        boolQueryBuilder.must(termQueryBuilder);
        sourceBuilder.query(boolQueryBuilder);
        sourceBuilder.from(0).size(10);
        request.source(sourceBuilder);
        SearchHit[] hits = restHighLevelClient.search(request, RequestOptions.DEFAULT).getHits().getHits();
        for (SearchHit hit : hits){
            System.out.println(hit.getSourceAsString());
            BuildingRecord buildingRecord = JSON.parseObject(hit.getSourceAsString(), BuildingRecord.class);
            System.out.println(JSON.toJSONString(buildingRecord));
            buildingRecords.add(buildingRecord);
        }
        return CommonResult.success(buildingRecords);
    }

    @ApiOperation(httpMethod = "GET", value = "根据备案号查询房建详情")
    @RequestMapping(value = "/info.ml", method = { RequestMethod.GET }, produces = "application/json;charset=UTF-8")
    @ApiImplicitParam(name = "remarkNo",value = "备案号",example = "202012068")
    public CommonResult<?> peopleHandCheck(@RequestParam(value = "remarkNo") String remarkNo) throws IOException {
         return CommonResult.success(buildingRecordService.findBuildingRecordInfo(remarkNo));
    }

}

package com.ml.room.terrace.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ml.room.common.result.CommonResult;
import com.ml.room.dao.IBuildingProjectItemDao;
import com.ml.room.repository.dto.BuildingRecordDto;
import com.ml.room.repository.entity.BuildingRecord;
import com.ml.room.service.IBuildingProjectItemService;
import com.ml.room.service.IBuildingProjectService;
import com.ml.room.service.IBuildingRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Map;

/**
 * @ClassName: RoomBuildingController
 * @Decription: 【云房平台】 - 房建信息控制层
 * @Author: IDai
 * @Date: 2021-10-19 16:11 星期二
 **/
@RestController
@RequestMapping(value = "/api/v1/building")
@Api(tags = "[云房平台] - 房建信息")
public class RoomBuildingController {
    @Autowired
    private IBuildingRecordService buildingRecordService;
    @Autowired
    private IBuildingProjectService buildingProjectService;
    @Autowired
    private IBuildingProjectItemService buildingProjectItemService;

    @PostMapping(value = "/list")
    @ApiOperation(value = "[云房平台] - 楼房列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "buildingName", value = "楼盘名称", example = "悦湖新著"),
            @ApiImplicitParam(name = "page", value = "页码", example = "1"),
            @ApiImplicitParam(name = "size", value = "数量", example = "10")
    })
    public CommonResult<IPage<BuildingRecord>> selectBildingList(@RequestBody BuildingRecordDto buildingRecordDto){
         return buildingRecordService.selectBuildingList(buildingRecordDto);
    }


    @PostMapping(value = "/hotWords")
    @ApiOperation(value = "[云房平台] - 热搜热词")
    public CommonResult<?> selectHotSearch(){
      return buildingRecordService.selectHotSearch();
    }

    @PostMapping(value = "/hotBuildings")
    @ApiOperation(value = "[云房平台] - 热搜楼盘")
    public CommonResult<?> selectHotSearchBuildingRecords(){
        return buildingRecordService.selectHotSearchBuildingRecords();
    }

    @PostMapping(value = "/globalInfo")
    @ApiOperation(value = "[云房平台] - 楼盘大致信息")
    @ApiImplicitParam(name = "buildingName", value = "楼盘名称", example = "悦湖新著")
    public CommonResult<?> findBuildingGlobalInfo(@RequestBody Map<String,Object> searchMap){
        return buildingRecordService.findBuildingGlobalInfo(searchMap);
    }

    @PostMapping(value = "/info")
    @ApiOperation(value = "[云房平台] - 楼盘细致详情")
    @ApiImplicitParam(name = "remarkNo",value = "备案号",example = "202012068")
    public CommonResult<?> findBuildingInfo(@RequestParam(value = "remarkNo") String remarkNo) throws IOException {
        return CommonResult.success(buildingRecordService.findBuildingRecordInfo(remarkNo));
    }


}

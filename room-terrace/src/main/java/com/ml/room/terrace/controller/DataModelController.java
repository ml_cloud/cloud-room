package com.ml.room.terrace.controller;

import com.ml.room.repository.entity.BuildingProject;
import com.ml.room.repository.entity.BuildingProjectItem;
import com.ml.room.repository.entity.BuildingRecord;
import com.ml.room.service.IBuildingProjectItemService;
import com.ml.room.service.IBuildingProjectService;
import com.ml.room.service.IBuildingRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: DataModelController
 * @Decription: 此处填写类文件说明
 * @Author: IDai
 * @Date: 2021-11-03 09:39 星期三
 **/
@Api(tags = "[云房平台] - 数据模型" )
@RestController
@RequestMapping(value = "/api/v1/model")
public class DataModelController {

    @Autowired
    private IBuildingRecordService buildingRecordService;
    @Autowired
    private IBuildingProjectService buildingProjectService;
    @Autowired
    private IBuildingProjectItemService buildingProjectItemService;

    @GetMapping(value = "/buildingRecord")
    @ApiOperation(value = "楼盘备案数据 - 示例")
    public BuildingRecord getBuildingRecord(){
        return buildingRecordService.getById("1430783405854945282");
    }

    @GetMapping(value = "/buildingProject")
    @ApiOperation(value = "楼盘备案数据 - 示例")
    public BuildingProject getBuildingProject(){
        return buildingProjectService.getById("1433000221469650945");
    }

    @GetMapping(value = "/buildingProjectItem")
    @ApiOperation(value = "楼盘备案数据 - 示例")
    public BuildingProjectItem getBuildingProjectItem(){
        return buildingProjectItemService.getById("1433700943446179841");
    }
}

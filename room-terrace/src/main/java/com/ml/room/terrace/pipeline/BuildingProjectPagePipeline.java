package com.ml.room.terrace.pipeline;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.ml.room.dao.IBuildingProjectDao;
import com.ml.room.dao.IBuildingRecordDao;
import com.ml.room.repository.entity.BuildingProject;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

/**
 * @ClassName: BuildingProjectPagePipeline
 * @Decription: 此处填写类文件说明
 * @Author: IDai
 * @Date: 2021-09-01 10:26 星期三
 **/
@Component
public class BuildingProjectPagePipeline implements Pipeline {
    @Autowired
    private IBuildingRecordDao buildingRecordDao;
    @Autowired
    private IBuildingProjectDao buildingProjectDao;

    /** 日志打印工具 **/
    private static final Logger logger = LoggerFactory.getLogger(BuildingProjectPagePipeline.class);

    @Override
    public void process(ResultItems resultItems, Task task) {
        BuildingProject buildingProject = (BuildingProject) resultItems.get("buildingProject");
        logger.info("【合肥新房】 - 房建项目 - 待入库的数据是，resultItems：{}", JSON.toJSONString(buildingProject));
        if (ObjectUtil.isNotEmpty(buildingProject)){
            int result = buildingProjectDao.isHaveThisBuildingProject(buildingProject.getProjectName());
            if (result == 0){
                result = buildingProjectDao.insert(buildingProject);
                if (result == 1){
                    buildingRecordDao.finishCollarProjectData(buildingProject.getRecordId());
                    logger.info("【合肥新房】 - 房建项目 - 处理结果：{}", "数据抓取完成！");
                }
            }else {
                logger.info("【合肥新房】 - 房建项目 - 处理结果：{}", "此数据重复！");
            }
        }else {
            //如果抽取的内容重复或者为null，爬取状态也要同步更新
            buildingRecordDao.finishCollarProjectData(resultItems.get("recordId"));
        }
    }
}

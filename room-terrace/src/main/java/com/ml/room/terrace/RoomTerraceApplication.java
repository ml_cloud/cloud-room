package com.ml.room.terrace;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

/**
 * @ClassName: RoomTerraceApplication
 * @Decription: 此处填写类文件说明
 * @Author: IDai
 * @Date: 2021-08-05 17:05 星期四
 **/
@SpringBootApplication
@ComponentScan(value = "com.ml.room")
public class RoomTerraceApplication {

    public static void main(String[] args) {
        SpringApplication.run(RoomTerraceApplication.class, args);
    }


}

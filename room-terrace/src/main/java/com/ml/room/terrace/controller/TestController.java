package com.ml.room.terrace.controller;

import com.alibaba.fastjson.JSONObject;
import com.ml.room.common.result.CommonResult;
import com.ml.room.common.utils.HttpUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: TestController
 * @Decription: 此处填写类文件说明
 * @Author: IDai
 * @Date: 2021-12-01 10:01 星期三
 **/
@RestController
@RequestMapping(value = "/test")
@Api(tags = "[测试平台] - 全局测试")
public class TestController {

    @GetMapping(value = "/pullMobile")
    @ApiOperation(value = "测试推送手机号")
    public CommonResult<?> testPullMobile(@RequestParam("mobile") String mobile){

        String url = "https://zdc.shenghuochuo.com/web/index.php";
        //建立一个NameValuePair数组，用于存储欲传送的参数
        List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
        //添加参数

        nameValuePairList.add(new BasicNameValuePair("r","ditie/jifen/index"));
        nameValuePairList.add(new BasicNameValuePair("m",mobile));
        try {
            Object object = HttpUtil.sendGet(url, nameValuePairList);
            if (object != null){
                if (object instanceof String){
                    //转化成JSON的时候出现异常，输出此字符串数据
                    return CommonResult.failed(object.toString());
                }else if (object instanceof JSONObject){
                    return CommonResult.failed((JSONObject) object);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return CommonResult.failed();
    }

}

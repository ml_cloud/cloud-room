package com.ml.room.terrace.component;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @ClassName: MyMetaObjectHandler
 * @Decription: mybatis-plus 全局时间注解
 * @Author: IDai
 * @Date: 2021-03-16 13:45 星期二
 **/
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        //使用mp实现添加操作，这个方法执行  createDate为字段  new Date()为现在时间  metaObject
        this.setFieldValByName("createDate",new Date(),metaObject);
        this.setFieldValByName("updateDate",new Date(),metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        //使用mp实现修改操作，这个方法执行
        this.setFieldValByName("updateDate",new Date(),metaObject);
    }
}

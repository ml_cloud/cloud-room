package com.ml.room.terrace.task;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ml.room.common.dto.RequestBuildingRecordDto;
import com.ml.room.common.dto.RequestProjectItemDto;
import com.ml.room.dao.IBuildingRecordDao;
import com.ml.room.dao.ISysCollarConfigDao;
import com.ml.room.repository.entity.BuildingRecord;
import com.ml.room.repository.entity.SysCollarConfig;
import com.ml.room.service.IBuildingRecordService;
import com.ml.room.terrace.pipeline.BuildingProjectItemPagePipeline;
import com.ml.room.terrace.pipeline.BuildingProjectPagePipeline;
import com.ml.room.terrace.pipeline.BuildingRecordPagePipeline;
import com.ml.room.terrace.process.BuildingProjectItemPageProcess;
import com.ml.room.terrace.process.BuildingProjectPageProcess;
import com.ml.room.terrace.process.BuildingRecordPageProcess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.model.HttpRequestBody;
import us.codecraft.webmagic.utils.HttpConstant;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: BuildingWebmagicTask
 * @Decription: 此处填写类文件说明
 * @Author: IDai
 * @Date: 2021-08-26 10:59 星期四
 **/
@Component
@EnableScheduling
public class BuildingWebMagicTask {
    @Autowired
    private ISysCollarConfigDao iSysCollarConfigDao;
    @Autowired
    private IBuildingRecordService buildingRecordService;
    @Autowired
    private IBuildingRecordDao buildingRecordDao;
    @Autowired
    private BuildingRecordPageProcess buildingRecordPageProcess;
    @Autowired
    private BuildingRecordPagePipeline buildingRecordPagePipeline;
    @Autowired
    private BuildingProjectPageProcess buildingProjectPageProcess;
    @Autowired
    private BuildingProjectPagePipeline buildingProjectPagePipeline;
    @Autowired
    private BuildingProjectItemPageProcess buildingProjectItemPageProcess;
    @Autowired
    private BuildingProjectItemPagePipeline buildingProjectItemPagePipeline;


    /** 日志打印工具 **/
    private static final Logger logger = LoggerFactory.getLogger(BuildingWebMagicTask.class);

    /**
     * method collarBuildingCommonData()
     * create by: IDai
     * description: 【合肥新房】 - 房屋备案 - 定时爬取
     * create time: 2021/8/30 0030
     * Param:
     * return:
     */
    @Scheduled(cron = "${webMagic.task.build.common.cron}")
    public void collarBuildingCommonData(){
        long startTime, endTime;
        startTime = System.currentTimeMillis();
        SysCollarConfig sysCollarConfig = iSysCollarConfigDao.selectById("1");
        if (!sysCollarConfig.getCollarPage().equals(5)){
            //一般备案的楼盘不会超过五十盘的，而且定时任务每天轮询一次，这里判断抓取的页数为 5就够了
            logger.info("【合肥新房】 - 房屋备案 - 数据抽取，抓取地址：{}，开始抓取.....",sysCollarConfig.getCollarUrl());
            //从"http://220.178.124.94:8010/fangjia/ws/DefaultList.aspx"开始抓
            try {
                Request request = new Request(sysCollarConfig.getCollarUrl());
                RequestBuildingRecordDto requestDto = new RequestBuildingRecordDto();
                //从第一页开始执行
                requestDto.set__EVENTARGUMENT("1");
                request.setRequestBody(HttpRequestBody.form(JSON.parseObject(JSON.toJSONString(requestDto), Map.class),"utf-8"));
                request.setMethod(HttpConstant.Method.POST);
                //创建并初始化执行页面抽取流程
                Spider.create(buildingRecordPageProcess)
                        //加入构造的接口访问逻辑
                        .addRequest(request)
                        //添加并执行页面抽取的数据处理
                        .addPipeline(buildingRecordPagePipeline)
                        //开启5个线程抓取
                        .thread(5)
                        //启动爬虫
                        .run();
                endTime = System.currentTimeMillis();
                logger.info("【合肥新房】 - 房屋备案，抓取结束，耗时：{}",((endTime - startTime) / 1000) + "秒");
            }catch (Exception e){
                logger.info("【合肥新房】 - 房屋备案，抓取出现异常e：{}",e.getMessage());
            }
        }else{
            //回滚抓取页码到第一页，定时任务执行结束。
            sysCollarConfig.setCollarPage(1);
            iSysCollarConfigDao.updateById(sysCollarConfig);
        }
    }


    @Scheduled(cron = "${webMagic.task.build.info.cron}")
    public void collarBuildingProjectData(){
        long startTime, endTime;
        startTime = System.currentTimeMillis();
        Map<String, String> collarMap = new HashMap<>();
        //设置要抓取的为房建项目数据
        collarMap.put("projectStatus","0");
        //首先，先查出未抓取的房屋备案信息。
        IPage<BuildingRecord> pageList = buildingRecordService.selectNotCollarList(collarMap);
        if (CollectionUtil.isNotEmpty(pageList.getRecords())){
            pageList.getRecords().stream().forEach(buildingRecord -> {
                logger.info("【合肥新房】 - 房建项目 - 数据抽取，抓取地址：{}，开始抓取.....",buildingRecord.getInfoUrl());
                try {
                    buildingProjectPageProcess.setRecordId(buildingRecord.getId());
                    Request request = new Request(buildingRecord.getInfoUrl());
                    RequestBuildingRecordDto requestDto = new RequestBuildingRecordDto();
                    requestDto.set__EVENTARGUMENT(buildingRecord.getCollarPage().toString());
                    request.setRequestBody(HttpRequestBody.form(JSON.parseObject(JSON.toJSONString(requestDto), Map.class),"utf-8"));
                    request.setMethod(HttpConstant.Method.POST);
                    //创建并初始化执行页面抽取流程
                    Spider.create(buildingProjectPageProcess)
                            //加入构造的接口访问逻辑
                            .addUrl(buildingRecord.getInfoUrl())
                            //添加并执行页面抽取的数据处理
                            .addPipeline(buildingProjectPagePipeline)
                            //开启5个线程抓取
                            .thread(5)
                            //启动爬虫
                            .run();
                }catch (Exception e){
                    logger.info("【合肥新房】 - 房建项目，抓取出现异常e：{}",e.getMessage());
                }
            });
        }
        endTime = System.currentTimeMillis();
        logger.info("【合肥新房】 - 房建项目，抓取结束，耗时：{}",((endTime - startTime) / 1000) + "秒");
    }


    @Scheduled(cron = "${webMagic.task.build.info.cron}")
    public void collarBuildingProjectItem(){
        long startTime, endTime;
        startTime = System.currentTimeMillis();
        Map<String, String> collarMap = new HashMap<>();
        //设置要抓取的为房建项目数据
        collarMap.put("projectStatus","1");
        collarMap.put("infoStatus","0");
        //首先，先查出未抓取的房屋备案信息。
        IPage<BuildingRecord> pageList = buildingRecordService.selectNotCollarList(collarMap);
        if (CollectionUtil.isNotEmpty(pageList.getRecords())){
            pageList.getRecords().stream().forEach(buildingRecord -> {
                logger.info("【合肥新房】 - 楼房信息 - 数据抽取，抓取地址：{}，当前页码：{}，开始抓取.....",buildingRecord.getInfoUrl(),buildingRecord.getCollarPage().toString());
                //当前备案楼房详情数据未抓取完成的时候，循环继续执行。
                Request request = new Request(buildingRecord.getInfoUrl());
                RequestProjectItemDto projectItemDto = new RequestProjectItemDto();
                int collarPage = 1;
                while (buildingRecord.getInfoStatus() == 0) {
                  projectItemDto.set__EVENTARGUMENT(String.valueOf(collarPage));
                  request.setRequestBody(HttpRequestBody.form(JSON.parseObject(JSON.toJSONString(projectItemDto), Map.class), "utf-8"));
                  request.setMethod(HttpConstant.Method.POST);
                  buildingProjectItemPageProcess.setRecordId(buildingRecord.getId());
                  collarPage++;
                      try {
                          //创建并初始化执行页面抽取流程
                          Spider.create(buildingProjectItemPageProcess)
                                  //加入构造的接口访问逻辑
                                  .addRequest(request)
                                  //添加并执行页面抽取的数据处理
                                  .addPipeline(buildingProjectItemPagePipeline)
                                  //开启5个线程抓取
                                  .thread(5)
                                  //启动爬虫
                                  .run();
                      } catch (Exception e) {
                          logger.info("【合肥新房】 - 房建项目，抓取出现异常e：{}", e.getMessage());
                      }
                      buildingRecord.setInfoStatus(buildingProjectItemPagePipeline.infoStatus);
                  }
                //重置infoStatus
                buildingProjectItemPagePipeline.setInfoStatus(0);
            });
        }
        endTime = System.currentTimeMillis();
        logger.info("【合肥新房】 - 楼房信息，抓取结束，耗时：{}",((endTime - startTime) / 1000) + "秒");
    }

}

package com.ml.room.terrace.process;

import cn.hutool.core.codec.Base64;
import com.alibaba.fastjson.JSON;
import com.ml.room.common.ffapi.FFocr;
import com.ml.room.common.utils.Base64Util;
import com.ml.room.common.utils.FileUtil;
import com.ml.room.common.utils.SeleniumUtil;
import com.ml.room.common.utils.XHocrInvokUtil;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Base64Utils;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

import java.io.File;
import java.util.Map;
import java.util.Set;

/**
 * @ClassName: InvoiceCheckPageProcess
 * @Decription: 发票查验 - 模拟操作
 * @Author: IDai
 * @Date: 2021-08-31 09:19 星期二
 **/
public class InvoiceCheckPageProcess implements PageProcessor {

    /** 日志打印工具 **/
    private static final Logger logger = LoggerFactory.getLogger(InvoiceCheckPageProcess.class);

    public static final String BASE_URL = "https://inv-veri.chinatax.gov.cn/";

    //设置抓取参数。详细配置见官方文档介绍 抓取网站的相关配置，包括编码、抓取间隔、重试次数等
    private final Site site = Site.me().setRetryTimes(3).setSleepTime(1000).setTimeOut(10000);
    //.setUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.65 Safari/537.31");

    @Override
    public void process(Page page) {
        InvoiceCheckPageProcess invoiceCheckPageProcess = new InvoiceCheckPageProcess();
        invoiceCheckPageProcess.peopleHandCheck();
    }

    @Override
    public Site getSite() {
        site.addCookie("fpcyhhbc","66634894");
        site.addCookie("JSESSIONID","h_mauOED23P_vvG7cbvd09fp4waLYhsAWJ0JZ-xCUQvVlGGAFdWA!-545055555");
        site.addCookie("_Jo0OQK","269F9D0F654AFE89B1AAA3CCED7DCFAA37BDE2FB26901FEEEA72D00DB1CBF84A68F1BE522EF564A354969A5BBAA7DEFD5566C23D69F49C7AFE4DB047A0C5CFFD1309B860DB80F26C941FB4BD9AD05BFA505FB4BD9AD05BFA505558E68D2DBDDE9ACGJ1Z1Ww==");
        return site.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1");
    }

    //使用 selenium 来模拟人工手输
    public void peopleHandCheck(){
        // 设置系统变量：
        WebDriver driver = null;
        try {
            //谷歌浏览器的驱动
            //System.setProperty("webdriver.chrome.driver","C:\\Program Files\\Google\\Chrome\\Application\\chromedriver.exe");
            //Edge浏览器的驱动
            //System.setProperty("webdriver.edge.driver","F:\\testDemo\\cloud-room\\room-common\\src\\main\\java\\com\\ml\\room\\common\\driver\\msedgedriver.exe");
            //IE浏览器的驱动
            System.setProperty("webdriver.ie.driver","F:\\testDemo\\cloud-room\\room-common\\src\\main\\java\\com\\ml\\room\\common\\driver\\IEDriverServer.exe");
            //System.setProperty("webdriver.edge.driver","F:\\testDemo\\cloud-room\\room-common\\src\\main\\java\\com\\ml\\room\\common\\driver\\msedgedriver.exe");
            driver = new InternetExplorerDriver();
            driver.get(BASE_URL);
            try {
                Thread.sleep(200);
                //判断元素节点是否存在
                boolean flag = SeleniumUtil.isJudgingElement(driver, By.id("details-button"));
                if (flag){
                    //如果出现安全提示页面，模拟点击继续访问
                    driver.findElement(By.id("details-button")).click();
                    driver.findElement(By.id("proceed-link")).click();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // 防止页面未能及时加载出来而设置一段时间延迟
            try {
                Thread.sleep(100);
                driver.findElement(By.id("fpdm")).clear();
                //在表单中填入准备查验的发票代码
                driver.findElement(By.id("fpdm")).sendKeys("3400201130");
                driver.findElement(By.id("fphm")).clear();
                //在表单中填入准备查验的发票号码
                driver.findElement(By.id("fphm")).sendKeys("10302337");
                driver.findElement(By.id("kprq")).clear();
                //异步获取验证码图片进行ocr解析分析验证码中的数据
                Thread.sleep(1000);
                String captchaCode = FfOcrAnalysisCaptchaCode(driver);
                //在表单中填入准备查验的开票日期
                driver.findElement(By.id("kprq")).sendKeys("20200905");
                driver.findElement(By.id("kjje")).clear();
                //在表单中填入准备查验的开具金额
                driver.findElement(By.id("kjje")).sendKeys("42.72");
                driver.findElement(By.id("yzm")).sendKeys(captchaCode);
                Thread.sleep(1000);
                driver.findElement(By.id("checkfp")).click();
                System.out.println("查验完成！");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (driver != null){
                driver.close();
            }
        }
    }

    public String FfOcrAnalysisCaptchaCode(WebDriver driver){
        //获取 验证码图片base64字节码，去除头部多余的字符。
        try {
            String base64Code = driver.findElement(By.id("yzm_unuse_img")).getAttribute("src").split(",")[1];
            String captchaText = driver.findElement(By.id("yzminfo")).getText();
            String head_info;
            switch (captchaText) {
                case "请输入验证码文字":
                    head_info = "{\"color\":\"00\"}";
                    break;
                case "请输入验证码图片中红色文字":
                    head_info = "{\"color\":\"01\"}";
                    break;
                case "请输入验证码图片中黄色文字":
                    head_info = "{\"color\":\"02\"}";
                    break;
                case "请输入验证码图片中蓝色文字":
                    head_info = "{\"color\":\"03\"}";
                    break;
                default:
                    head_info = null;
            }
            String ocrStr = null;
            do {
                ocrStr = new FFocr().ocr_api(base64Code, head_info);
            } while (StringUtils.isEmpty(ocrStr));
            return ocrStr;
        }catch (Exception e){
            if (driver != null){
                driver.close();
            }
            e.printStackTrace();
        }finally {

        }
        return null;
    }

    public static void main(String[] args) {
//        InvoiceCheckPageProcess invoiceCheckPageProcess = new InvoiceCheckPageProcess();
//        invoiceCheckPageProcess.login();
//        invoiceCheckPageProcess.peopleHandCheck();
        Spider.create(new InvoiceCheckPageProcess())
                .addUrl(BASE_URL)
                .run();
    }

}

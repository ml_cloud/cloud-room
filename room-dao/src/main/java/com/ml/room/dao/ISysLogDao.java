package com.ml.room.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ml.room.repository.entity.SysLog;

/**
 * @ClassName: ISysLogDao
 * @Decription: 此处填写类文件说明
 * @Author: IDai
 * @Date: 2021-08-06 11:44 星期五
 **/
public interface ISysLogDao extends BaseMapper<SysLog> {
}

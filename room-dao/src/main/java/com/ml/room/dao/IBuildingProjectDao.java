package com.ml.room.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ml.room.repository.entity.BuildingProject;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @ClassName: IBuildingProjectDao
 * @Decription: 【云房平台】 - 房建项目 - dao接口层
 * @Author: IDai
 * @Date: 2021-08-27 11:48 星期五
 **/
public interface IBuildingProjectDao extends BaseMapper<BuildingProject> {

    @Select("SELECT COUNT(1) FROM `building_project` WHERE project_name = #{projectName}")
    int isHaveThisBuildingProject(@Param(value = "projectName") String projectName);

    @Select("SELECT id FROM `building_project` WHERE project_name = #{projectName}")
    String findIdByProjectName(@Param(value = "projectName") String projectName);

    /**
     * method
     * create by: IDai
     * description: 根据备案楼盘id查询房建项目
     * create time: 2021/11/3 0003
     */
    @Select("SELECT * FROM `building_project` WHERE record_id = #{recordId}")
    BuildingProject selectOneByRecordId(@Param(value = "recordId") String recordId);

}

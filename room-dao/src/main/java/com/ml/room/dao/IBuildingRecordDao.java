package com.ml.room.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ml.room.repository.entity.BuildingRecord;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * @ClassName: IBuildingRecordDao
 * @Decription: 【云房平台】 - 房屋备案 - dao接口层
 * @Author: IDai
 * @Date: 2021-08-13 14:45 星期五
 **/
public interface IBuildingRecordDao extends BaseMapper<BuildingRecord> {
    /**
     * method
     * create by: IDai
     * description: 根据备案号判断是否已有此房屋备案信息
     * create time: 2021/8/26 0026
     */
    @Select("SELECT COUNT(1) FROM `building_record` WHERE remark_no = #{remarkNo}")
    int isHaveThisBuildingRecord(@Param(value = "remarkNo") String remarkNo);

    @Update("UPDATE `building_record` SET project_status = 1 WHERE id = #{id}")
    int finishCollarProjectData(@Param(value = "id") String id);

    @Update("UPDATE `building_record` SET info_status = 1 WHERE id = #{id}")
    int finishCollarProjectItemData(@Param(value = "id") String id);

    @Update("UPDATE `building_record` SET collar_page = collar_page + 1 WHERE id = #{id}")
    int updateCollarPageById(@Param(value = "id") String id);

    @Update("UPDATE `building_record` SET info_url = #{infoUrl} WHERE remark_no = #{remarkNo}")
    int updateinfoUrlByRemarkNo(@Param(value = "remarkNo") String remarkNo, @Param(value = "infoUrl") String infoUrl);

}

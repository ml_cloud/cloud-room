package com.ml.room.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ml.room.repository.entity.BuildingProjectItem;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @ClassName: IBuildingProjectItemDao
 * @Decription: 【云房平台】 - 房建项目楼房 - dao接口层
 * @Author: IDai
 * @Date: 2021-08-27 15:57 星期五
 **/
public interface IBuildingProjectItemDao extends BaseMapper<BuildingProjectItem> {
    @Select("SELECT COUNT(1) FROM `building_project_item` WHERE record_id = #{recordId} AND building_no = #{buildingNo} AND room_number = #{roomNumber}")
    int isHaveThisBuildingProjectItem(@Param(value = "recordId") String recordId,@Param(value = "buildingNo") String buildingNo,@Param(value = "roomNumber") String roomNumber);
}

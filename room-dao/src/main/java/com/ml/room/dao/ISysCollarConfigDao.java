package com.ml.room.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ml.room.repository.entity.SysCollarConfig;

/**
 * @ClassName: ISysCollarConfigDao
 * @Decription: 【云房平台】 - 系统抓取配置
 * @Author: IDai
 * @Date: 2021-08-26 10:50 星期四
 **/
public interface ISysCollarConfigDao extends BaseMapper<SysCollarConfig> {



}

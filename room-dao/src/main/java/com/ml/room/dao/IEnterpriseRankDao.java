package com.ml.room.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ml.room.repository.entity.EnterpriseRank;

/**
 * @ClassName: IEnterpriseRankDao
 * @Decription: 【云房平台】 - 企业排行 - dao层
 * @Author: IDai
 * @Date: 2021-08-27 16:08 星期五
 **/
public interface IEnterpriseRankDao extends BaseMapper<EnterpriseRank> {
}

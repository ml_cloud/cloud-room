package com.ml.room.common.constant;

/**
 * @ClassName: RequestConstant
 * @Decription: 此处填写类文件说明
 * @Author: IDai
 * @Date: 2021-08-31 11:48 星期二
 **/
public class RequestConstant {
    /**
     * description: 薪火OCR
     * （通用OCR、身份证、车牌、营业执照、驾照、行驶证、银行卡）
     */
    public static final String XH_COMMON_SERVER_URL = "http://112.30.110.198:28901/ocr";//薪火OCR识别serverUrl地址
    public static final String XH_INVOICE_SERVER_URL = "http://api.xinhuokj.com:40072/ocr";//薪火OCR识别serverUrl地址（增值税发票、多票据、港澳台通行证、表格）
    public static final String XH_TOKEN = "f37d4f3ad8b8a9b1c56a4c4ae897fb5e";//薪火OCR识别token值
}

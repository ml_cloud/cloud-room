package com.ml.room.common.constant;

import java.io.Serializable;

/**
 * @ClassName: RedisConstant
 * @Decription: redis 相关的配置
 * @Author: IDai
 * @Date: 2021-10-20 11:52 星期三
 **/
public class RedisConstant implements Serializable {

    /** 备案楼盘热搜前缀 **/
    public static final String BUILDING_HOT_SEARCH_KEY = "building_hot_search_";

    /** 开发商热搜前缀 **/
    public static final String DEVELOPERS_HOT_SEARCH_KEY = "developers_hot_search_";

    /** 房屋热搜前缀 **/
    public static final String ROOM_HOT_SEARCH_KEY = "ROOM_hot_search_";


}

package com.ml.room.common.test;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @ClassName: UntiDemo
 * @Decription: 此处填写类文件说明
 * @Author: IDai
 * @Date: 2021-08-13 15:29 星期五
 **/
public class UntiDemo {

       /** 日志打印工具 **/
       private static final Logger logger = LoggerFactory.getLogger(UntiDemo.class);
       private static void testBuildingRecordPageProcess(){
            class BuildingRecordPageProcess implements PageProcessor {
               // 正则表达式\\. \\转义java中的\ \.转义正则中的.
               // 主域名
               public static final String URL = "http://drc.hefei.gov.cn/jggl/jggs/spzzmmbjcx/";
               public static final String BASE_URL = "http://drc.hefei.gov.cn/jggl/jggs/spzzmmbjcx/index.html";
               public static final String PAGE_URL = "http://58.210.114.86/bbs/forum.php?mod=forumdisplay&fid=2&page=1";
               //设置抓取参数。详细配置见官方文档介绍 抓取网站的相关配置，包括编码、抓取间隔、重试次数等
               private final Site site = Site.me()
                       .setDomain(BASE_URL)
                       .setSleepTime(1000)
                       //重试次数
                       .setRetryTimes(30)
                       //编码格式
                       .setCharset("utf-8")
                       //超时设置
                       .setTimeOut(5000);
               //.setUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.65 Safari/537.31");


               @Override
               public Site getSite() {
                   return site;
               }

               @Override
               public void process(Page page) {

               }

//               public void main(String[] args) {
//                   Spider.create(new BuildingRecordPageProcess())
//                           //从"https://github.com/code4craft"开始抓
//                           .addUrl("URL")
//                           //开启5个线程抓取
//                           .thread(5)
//                           //启动爬虫
//                           .run();
//                   System.out.println("数据开始抓取中......");
//               }

           }
       }
    public static void main(String[] args) {
        String s = "02156430,02156432,02156439,02156438,02156435,02156442,02156448";
        List<String> list = Arrays.asList(StringUtils.split(s, ","));
        Collections.sort(list);
        String invoiceNo = StringUtils.join(list, ",");
        System.out.println(invoiceNo);
    }

}

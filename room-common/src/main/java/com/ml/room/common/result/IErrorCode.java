package com.ml.room.common.result;

/**
 * @ClassName: CommonResult
 * @Decription: 封装api的错误码
 * @Author: IDai
 * @Date: 2020-06-10 09:08 星期三
 **/
public interface IErrorCode {

    long getCode();

    String getMessage();
}

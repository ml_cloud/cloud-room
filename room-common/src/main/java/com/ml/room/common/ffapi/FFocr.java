package com.ml.room.common.ffapi;

import org.springframework.stereotype.Component;

/**
 * @ClassName:FFocr
 * @Decription:菲菲打码OCR接口实现
 * @Author: yening
 * @Date:2021-08-05 10:48 星期四
 **/
@Component
public class FFocr {
    public String ocr_api(String img_base64data, String code_color) throws Exception {
        /**
         *@method ocr_api
         * create by: yening
         * description:
         * create time: 2021-08-05 10:48
         * @param:[img_base64data:base64格式的图片数据, code_color：验证码的颜色]
         * @return java.lang.String
         */
        Api api = new Api();
        String app_id = "302317";
        String app_key = "BlyvpmQWMFt8o1a6DU+1kO5BO6o8xVov";
        String pd_id = "131062";
        String pd_key = "SBfemSQcPRRCE1JOS8tuTFSUPALL350E";
        // 对象生成之后，在任何操作之前，需要先调用初始化接口
        api.Init(app_id, app_key, pd_id, pd_key, code_color);
        // 查询余额
        Util.HttpResp resp;
        // resp = api.QueryBalc();   // 查询余额返回详细信息
        // System.out.printf("query balc!ret: %d cust: %f err: %s reqid: %s pred: %s\n", resp.ret_code, resp.cust_val, resp.err_msg, resp.req_id, resp.pred_resl);
        double balance = api.QueryBalcExtend();    // 直接返回余额结果
//        System.out.println("Balance:" + balance);

        // 具体类型可以查看官方网站的价格页选择具体的类型，不清楚类型的，可以咨询客服，自定义类型80400；30400:4位数字英文
        String pred_type = "80400";
        // 通过文件进行验证码识别,请使用自己的图片文件替换
//        String img_file = "G:\\1.file\\3.Project\\PythonPorject\\rpa\\asd.png";
        // 多网站类型时，需要增加src_url参数，具体请参考api文档: http://docs.fateadm.com/web/#/1?page_id=6

        resp = api.PredictFromFile(pred_type, img_base64data);  // 返回识别结果的详细信息
//        System.out.printf("predict from file!ret: %d cust: %f err: %s reqid: %s pred: %s\n", resp.ret_code, resp.cust_val, resp.err_msg, resp.req_id, resp.pred_resl);
        boolean jflag = true;
        if (jflag) {
            // 识别的结果如果与预期不符，可以调用这个接口将预期不符的订单退款
            // 退款仅在正常识别出结果后，无法通过网站验证的情况，请勿非法或者滥用，否则可能进行封号处理
            int ret = api.JusticeExtend(resp.req_id); // 直接返回是否成功
            if (ret == 0) System.out.println("Justice Success!");
            else System.out.println("Justice fail! code:" + ret);

//            resp = api.Justice(resp.req_id); // 返回完整信息
//            System.out.printf("justice !ret: %d cust: %f err: %s reqid: %s pred: %s\n", resp.ret_code, resp.cust_val, resp.err_msg, resp.req_id, resp.pred_resl);
        }
            return resp.pred_resl;

        }

    }

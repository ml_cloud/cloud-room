package com.ml.room.common.utils;

import com.alibaba.fastjson.JSON;
import com.ml.room.common.constant.RequestConstant;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: XHocrInvokUtil
 * @Decription: 此处填写类文件说明
 * @Author: IDai
 * @Date: 2021-08-31 11:46 星期二
 **/
public class XHocrInvokUtil {

    /** 日志打印工具 **/
    private static final Logger logger = LoggerFactory.getLogger(XHocrInvokUtil.class);

    public static Map<String, Object> getOcrCaptchaText(byte[] imgData) throws Exception {
        Map<String, Object> returnMap = new HashMap<String, Object>(16);
        try {
            //读取待测试的图片
            //byte[] imgData = FileUtil.readFileByBytes("C:\\Users\\Administrator\\Desktop\\image\\8.jpg");
            //把图片转成base64编码
            String imgStr = Base64Util.encode(imgData);
            //把参数拼接成json字符串
            String params = "{\"type\":\"0\", \"image\": \"" + imgStr + "\", \"token\":\"" + RequestConstant.XH_TOKEN + "\" } ";
            logger.info("[薪火ocr识别] - 请求报文：{}", JSON.toJSON(params));
            //向服务端发送http请求，返回识别结果
            String response = HttpUtil.postGeneralUrl(RequestConstant.XH_COMMON_SERVER_URL, "application/json", params, "UTF-8");
            String returnCode = JSON.parseObject(response).getString("code");
            String returnCostTime = JSON.parseObject(response).getString("cost_time");
            String returnMessage = JSON.parseObject(response).getString("message");
            String returnContent = JSON.parseObject(response).getString("result");
            String successCode = "200";
            if (StringUtils.equals(successCode, returnCode)) {

            }
            returnMap.put("returnCode", returnCode);
            returnMap.put("returnCostTime", returnCostTime);
            returnMap.put("returnMessage", returnMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("[薪火ocr识别] - 响应报文：{}", JSON.toJSON(returnMap));
        return returnMap;
    }
}

package com.ml.room.common.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * @ClassName: SeleniumUtil
 * @Decription: 此处填写类文件说明
 * @Author: IDai
 * @Date: 2021-09-01 09:25 星期三
 **/
public class SeleniumUtil {
    /**
     * 判断某个元素是否存在
     */
    public static boolean isJudgingElement(WebDriver webDriver, By by) {
        try {
            webDriver.findElement(by);
            return true;
        } catch (Exception e) {
            System.out.println("不存在此元素");
            return false;
        }
    }
}

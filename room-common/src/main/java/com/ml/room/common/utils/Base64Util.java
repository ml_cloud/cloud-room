package com.ml.room.common.utils;

import org.apache.commons.lang3.StringUtils;
import sun.misc.BASE64Decoder;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Base64 工具类
 *
 * @author Administrator
 */
public class Base64Util {
    private static final char last2byte = (char) Integer.parseInt("00000011", 2);
    private static final char last4byte = (char) Integer.parseInt("00001111", 2);
    private static final char last6byte = (char) Integer.parseInt("00111111", 2);
    private static final char lead6byte = (char) Integer.parseInt("11111100", 2);
    private static final char lead4byte = (char) Integer.parseInt("11110000", 2);
    private static final char lead2byte = (char) Integer.parseInt("11000000", 2);
    private static final char[] encodeTable = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

    public Base64Util() {
    }

    public static String encode(byte[] from) {
        StringBuilder to = new StringBuilder((int) ((double) from.length * 1.34D) + 3);
        int num = 0;
        char currentByte = 0;

        int i;
        for (i = 0; i < from.length; ++i) {
            for (num %= 8; num < 8; num += 6) {
                switch (num) {
                    case 0:
                        currentByte = (char) (from[i] & lead6byte);
                        currentByte = (char) (currentByte >>> 2);
                    case 1:
                    case 3:
                    case 5:
                    default:
                        break;
                    case 2:
                        currentByte = (char) (from[i] & last6byte);
                        break;
                    case 4:
                        currentByte = (char) (from[i] & last4byte);
                        currentByte = (char) (currentByte << 2);
                        if (i + 1 < from.length) {
                            currentByte = (char) (currentByte | (from[i + 1] & lead2byte) >>> 6);
                        }
                        break;
                    case 6:
                        currentByte = (char) (from[i] & last2byte);
                        currentByte = (char) (currentByte << 4);
                        if (i + 1 < from.length) {
                            currentByte = (char) (currentByte | (from[i + 1] & lead4byte) >>> 4);
                        }
                }

                to.append(encodeTable[currentByte]);
            }
        }

        if (to.length() % 4 != 0) {
            for (i = 4 - to.length() % 4; i > 0; --i) {
                to.append("=");
            }
        }

        return to.toString();
    }

    /**
     * base64转图片
     *
     * @param base64str base64码
     * @param fileRoot  图片路径
     * @return
     */
    public static Map<String, Object> GenerateImage(String base64str, String fileRoot) {
        Map map = new HashMap();
        //对字节数组字符串进行Base64解码并生成图片
        if (base64str == null) {
            map.put("", "");
            return map;
        }
        BASE64Decoder decoder = new BASE64Decoder();
        try {
            String ext = StringUtils.defaultIfEmpty(StringUtils.substringBetween(base64str, "data:image/", ";"), "jpg");//图片后缀
            String base64ImgData = StringUtils.substringBetween(base64str, "base64,", "=");//图片数据
            File file = new File(fileRoot);
            if (!file.exists()) {//文件根目录不存在时创建
                new File(fileRoot).mkdirs();
            }
            String fileName = SnowflakeIdUtil.generateId() + "." + ext;
            String filePath = fileRoot + fileName;//图片路径
            //Base64解码
            byte[] b = decoder.decodeBuffer(base64ImgData);
            for (int i = 0; i < b.length; ++i) {
                //调整异常数据
                if (b[i] < 0) {
                    b[i] += 256;
                }
            }
            //生成jpeg图片
            FileOutputStream os = new FileOutputStream(filePath);
            os.write(b);
            os.close();
            File fileData = new File(filePath);
            map.put("fileRoot", fileRoot);
            map.put("fileName", fileName);
            map.put("ext", ext);
            map.put("fileLength", fileData.length());
            return map;
        } catch (Exception e) {
            return map;
        }
    }
}

package com.ml.room.common.result;

/**
 * @ClassName: CommonResult
 * @Decription: 枚举一下常用的api操作码
 * @Author: IDai
 * @Date: 2020-06-10 09:08 星期三
 **/
public enum ResultCode implements IErrorCode {
    //成功状态
    SUCCESS(200, "操作成功"),
    //失败状态
    FAILED(500, "操作失败"),
    VALIDATE_FAILED(402, "参数检验失败"),
    UNAUTHORIZED(401, "暂未登录或token已经过期"),
    NO_THIS_ACCOUNT(503, "无此账号,请先注册!"),
    FORBIDDEN(403, "没有相关权限"),
    NOT_FOUND_ERROR(404, "找不到资源路径");

    private long code;
    private String message;

    private ResultCode(long code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public long getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}

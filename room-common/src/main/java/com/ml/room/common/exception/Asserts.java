package com.ml.room.common.exception;


import com.ml.room.common.result.IErrorCode;

/**
 * @ClassName: Asserts
 * @Decription: 断言处理类，用于抛出各种API异常
 * @Author: IDai
 * @Date: 2020-06-10 09:58 星期三
 **/
public class Asserts {

    public static void fail(String message) {
        throw new ApiException(message);
    }

    public static void fail(IErrorCode errorCode) {
        throw new ApiException(errorCode);
    }
}

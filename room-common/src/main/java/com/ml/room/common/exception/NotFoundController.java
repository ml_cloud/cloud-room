package com.ml.room.common.exception;

/**
 * @ClassName: NotFoundController
 * @Decription: 自定义处理404错误
 * @Author: IDai
 * @Date: 2021-01-08 15:28 星期五
 **/
//@Controller
//@RequestMapping("${server.error.path:${error.path:/error}}")
public class NotFoundController {//extends BasicErrorController {

//    /**
//     * 日志打印工具
//     **/
//    private Logger logger = LoggerFactory.getLogger(NotFoundController.class);
//
//    @Value("${server.error.path:${error.path:/error}}")
//    private String path;
//
//    public NotFoundController(ServerProperties serverProperties) {
//        super(new DefaultErrorAttributes(), serverProperties.getError());
//    }
//
//    /**
//     * 覆盖默认的JSON响应
//     */
//    @Override
//    public ResponseEntity<Map<String, Object>> error(HttpServletRequest request) {
//        HttpStatus status = getStatus(request);
//        Map<String, Object> map = new HashMap<String, Object>(16);
//        Map<String, Object> originalMsgMap = getErrorAttributes(request, isIncludeStackTrace(request, MediaType.ALL));
//        String path = (String) originalMsgMap.get("path");
//        String error = (String) originalMsgMap.get("error");
//        String message = (String) originalMsgMap.get("message");
//        StringJoiner joiner = new StringJoiner(",", "[", "]");
//        joiner.add(path).add(error).add(message);
//        map.put("code", "404");
//        map.put("data", joiner.toString());
//        map.put("message", "请求资源路径异常！");
//        logger.error(message, isIncludeStackTrace(request, MediaType.ALL));
//        return new ResponseEntity<Map<String, Object>>(map, status);
//    }


}
